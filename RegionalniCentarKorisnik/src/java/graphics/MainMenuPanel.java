package graphics;

import regionalnicentarkorisnik.Main;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

//size (500,400)
public class MainMenuPanel extends javax.swing.JPanel {
    
    public static int width = 500;
    public static int height = 400;
    
    //buttons
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    
    //useful labels
    private javax.swing.JLabel pasosLabel;
    
    //useless labels
    private javax.swing.JLabel emptyLabel1;
    private javax.swing.JLabel emptyLabel2;
    private javax.swing.JLabel emptyLabel3;
    private javax.swing.JLabel emptyLabel4;
    private javax.swing.JLabel emptyLabel5;
    private javax.swing.JLabel emptyLabel6;
    private javax.swing.JLabel emptyLabel7;
    private javax.swing.JLabel emptyLabel8;
    private javax.swing.JLabel emptyLabel9;
    private javax.swing.JLabel emptyLabel10;
    private javax.swing.JLabel emptyLabel11;
    private javax.swing.JLabel emptyLabel12;
    
    public MainMenuPanel() 
    {
        initComponents();
    }

    @SuppressWarnings("unchecked")                          
    private void initComponents() {

        pasosLabel = new javax.swing.JLabel("Softver za izradu pasosa",SwingConstants.CENTER);
        pasosLabel.setFont(new Font("Arial", Font.BOLD, 13));
        emptyLabel1 = new javax.swing.JLabel();
        emptyLabel2 = new javax.swing.JLabel();
        emptyLabel3 = new javax.swing.JLabel();
        emptyLabel3 = new javax.swing.JLabel();
        emptyLabel4 = new javax.swing.JLabel();
        emptyLabel5 = new javax.swing.JLabel();
        emptyLabel6 = new javax.swing.JLabel();
        emptyLabel7 = new javax.swing.JLabel();
        emptyLabel8 = new javax.swing.JLabel();
        emptyLabel9 = new javax.swing.JLabel();
        emptyLabel10 = new javax.swing.JLabel();
        emptyLabel11 = new javax.swing.JLabel();
        emptyLabel12 = new javax.swing.JLabel();
        
        jButton1 = new javax.swing.JButton();
        jButton1.setMargin(new Insets(0,0,0,0));
        jButton2 = new javax.swing.JButton();
        jButton2.setMargin(new Insets(0,0,0,0));
        jButton3 = new javax.swing.JButton();
        jButton3.setMargin(new Insets(0,0,0,0));
        jButton4 = new javax.swing.JButton();
        jButton4.setMargin(new Insets(0,0,0,0));
        
        
        setLayout(new java.awt.GridLayout(6, 5, 0, 7));
        add(emptyLabel1);
        add(pasosLabel);

        
        add(emptyLabel2);
        add(emptyLabel3);

        jButton1.setText("Podnosenje zahteva");
        add(jButton1);  
        add(emptyLabel4);
        add(emptyLabel5);

        jButton2.setText("Provera statusa zahteva");
        add(jButton2);
        add(emptyLabel6);
        add(emptyLabel7);

        jButton3.setText("Slobodni termini");
        add(jButton3);
        add(emptyLabel8);
        add(emptyLabel9);

        jButton4.setText("Izlaz");
        add(jButton4);
        add(emptyLabel10);
        add(emptyLabel11);

        add(emptyLabel12);

        jButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.changeCurrPanel(Main.dataEntryScreen, DataEntryPanel.width, DataEntryPanel.height);
            }
        });
        
        jButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.changeCurrPanel(Main.requestStatePanel, RequestStatePanel.width, RequestStatePanel.height);
            }
        });
        
        jButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.changeCurrPanel(Main.freeTimeSlotsPanel, FreeTimeSlotsPanel.width, FreeTimeSlotsPanel.height);
            }
        });
        
        jButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0); 
            }
        });        
    }
}
