/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverReqClasses;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DocumentResponse")
public class DocumentResponse implements Serializable
{
    private static final long serialVersionUID = 6529685098267757680L;
    
    private String id; //min i max 12
    private String JMBG; //min i max 13
    private String ime; //max 50
    private String prezime; // max 50
    private String imeMajke;
    private String imeOca;
    private String prezimeMajke;
    private String prezimeOca;
    private String pol;
    private Date datumRodjenja;
    private String nacionalnost;
    private String profesija;
    private String bracnoStanje;
    private String opstinaPrebivalista;
    private String ulicaPrebivalista;    
    private String brojPrebivalista;
    private String status;

    public DocumentResponse(String id, String JMBG, String ime, String prezime, String imeMajke, String imeOca, String prezimeMajke, String prezimeOca, String pol, Date datumRodjenja, String nacionalnost, String profesija, String bracnoStanje, String opstinaPrebivalista, String ulicaPrebivalista, String brojPrebivalista, String status) {
        this.id = id;
        this.JMBG = JMBG;
        this.ime = ime;
        this.prezime = prezime;
        this.imeMajke = imeMajke;
        this.imeOca = imeOca;
        this.prezimeMajke = prezimeMajke;
        this.prezimeOca = prezimeOca;
        this.pol = pol;
        this.datumRodjenja = datumRodjenja;
        this.nacionalnost = nacionalnost;
        this.profesija = profesija;
        this.bracnoStanje = bracnoStanje;
        this.opstinaPrebivalista = opstinaPrebivalista;
        this.ulicaPrebivalista = ulicaPrebivalista;
        this.brojPrebivalista = brojPrebivalista;
        this.status = status;
    }

    public DocumentResponse() {
    }

    public String getId() {
        return id;
    }

    public String getJMBGl() {
        return JMBG;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getImeMajke() {
        return imeMajke;
    }

    public String getImeOca() {
        return imeOca;
    }

    public String getPrezimeMajke() {
        return prezimeMajke;
    }

    public String getPrezimeOca() {
        return prezimeOca;
    }

    public String getPol() {
        return pol;
    }

    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    public String getNacionalnost() {
        return nacionalnost;
    }

    public String getProfesija() {
        return profesija;
    }

    public String getBracnoStanje() {
        return bracnoStanje;
    }

    public String getOpstinaPrebivalista() {
        return opstinaPrebivalista;
    }

    public String getUlicaPrebivalista() {
        return ulicaPrebivalista;
    }

    public String getBrojPrebivalista() {
        return brojPrebivalista;
    }

    public String getStatus() {
        return status;
    }

    
    @XmlElement(name="id")
    public void setId(String id) {
        this.id = id;
    }

    @XmlElement(name="JMBG")
    public void setJMBGl(String JMBGl) {
        this.JMBG = JMBGl;
    }

    @XmlElement(name="ime")
    public void setIme(String ime) {
        this.ime = ime;
    }

    @XmlElement(name="prezime")
    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    @XmlElement(name="imeMajke")
    public void setImeMajke(String imeMajke) {
        this.imeMajke = imeMajke;
    }

    @XmlElement(name="imeOca")
    public void setImeOca(String imeOca) {
        this.imeOca = imeOca;
    }

    @XmlElement(name="prezimeMajke")
    public void setPrezimeMajke(String prezimeMajke) {
        this.prezimeMajke = prezimeMajke;
    }

    @XmlElement(name="prezimeOca")
    public void setPrezimeOca(String prezimeOca) {
        this.prezimeOca = prezimeOca;
    }

    @XmlElement(name="pol")
    public void setPol(String pol) {
        this.pol = pol;
    }

    @XmlElement(name="datumRodjenja")
    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    @XmlElement(name="nacionalnost")
    public void setNacionalnost(String nacionalnost) {
        this.nacionalnost = nacionalnost;
    }

    @XmlElement(name="profesija")
    public void setProfesija(String profesija) {
        this.profesija = profesija;
    }

    @XmlElement(name="bracnoStanje")
    public void setBracnoStanje(String bracnoStanje) {
        this.bracnoStanje = bracnoStanje;
    }

    @XmlElement(name="opstinaPrebivalista")
    public void setOpstinaPrebivalista(String opstinaPrebivalista) {
        this.opstinaPrebivalista = opstinaPrebivalista;
    }

    @XmlElement(name="ulicaPrebivalista")
    public void setUlicaPrebivalista(String ulicaPrebivalista) {
        this.ulicaPrebivalista = ulicaPrebivalista;
    }
    
    @XmlElement(name="brojPrebivalista")
    public void setBrojPrebivalista(String brojPrebivalista) {
        this.brojPrebivalista = brojPrebivalista;
    }

    @XmlElement(name="status")
    public void setStatus(String status) {
        this.status = status;
    }
    
    @Override
    public String toString() {
        return "DocumentResponse{" + "id=" + id + ", JMBG=" + JMBG + ", ime=" + ime + ", prezime=" + prezime + ", imeMajke=" + imeMajke + ", imeOca=" + imeOca + ", prezimeMajke=" + prezimeMajke + ", prezimeOca=" + prezimeOca + ", pol=" + pol + ", datumRodjenja=" + datumRodjenja + ", nacionalnost=" + nacionalnost + ", profesija=" + profesija + ", bracnoStanje=" + bracnoStanje + ", opstinaPrebivalista=" + opstinaPrebivalista + ", ulicaPrebivalista=" + ulicaPrebivalista + ", brojPrebivalista=" + brojPrebivalista + ", status=" + status + '}';
    }
    
    
}
