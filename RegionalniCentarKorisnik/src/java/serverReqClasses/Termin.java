/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverReqClasses;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Termin")
public class Termin implements Serializable
{
  //  private static final long serialVersionUID = 4L;
    private String periodOd;
    private String periodDo;

    public Termin() {
    }

    public Termin(String periodOd, String periodDo) {
        this.periodOd = periodOd;
        this.periodDo = periodDo;
    }

    public String getPeriodOd() {
        return periodOd;
    }

    @XmlElement(name="periodOd")
    public void setPeriodOd(String periodOd) {
        this.periodOd = periodOd;
    }

    public String getPeriodDo() {
        return periodDo;
    }

    @XmlElement(name="periodDo")
    public void setPeriodDo(String periodDo) {
        this.periodDo = periodDo;
    }

    @Override
    public String toString() {
        return "Od =" + periodOd + "  Do =" + periodDo;
    }
}
