/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regionalnicentarkorisnik;

import javax.swing.JFrame;
import javax.swing.JPanel;
import graphics.*;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.Message;
import javax.jms.Topic;

/**
 *
 * @author Vol
 */
public class Main {

    @Resource(lookup = "jms/__defaultConnectionFactory")
    public static ConnectionFactory connectionFactory;
    
    @Resource(lookup = "idTopic")
    public static Topic idTopic;
    
    public static JMSContext context;
    
    public static JFrame frame;  
    
    public static final MainMenuPanel mainMenuScreen = new MainMenuPanel();
    public static final DataEntryPanel dataEntryScreen = new DataEntryPanel();
    public static final FreeTimeSlotsPanel freeTimeSlotsPanel = new FreeTimeSlotsPanel();
    public static final RequestStatePanel requestStatePanel = new RequestStatePanel();
    private static JPanel currentPanel = mainMenuScreen;
    
    
    public static void changeCurrPanel(JPanel nextPanel, int width, int height)
    {
        frame.remove(currentPanel);
        frame.add(nextPanel);
        currentPanel = nextPanel;
        frame.repaint();
        frame.setSize(width,height);
    }
    
    public static void rep()
    {
        currentPanel.repaint();
    }
    
    public static void main(String[] args) 
    {
        TimerListener tl = new TimerListener();
        tl.start();
        context = connectionFactory.createContext();//timer.connection.createContext();
        
        frame = new JFrame("Regionalni centar");
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500,400);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.add(mainMenuScreen);     
//        JMSConsumer consumer = context.createConsumer(idTopic);
//        while (true)
//        {
//            Message msg = consumer.receive();
//            System.out.println("received");
//        }
    }
    
}
