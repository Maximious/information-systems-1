/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regionalnicentarkorisnik;

import graphics.RequestStatePanel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.swing.JDialog;
import javax.swing.JLabel;
import serverReqClasses.DocumentResponse;

public class TimerListener extends Thread
{
    @Override
    public void run()
    {
        while(true)
        {
            JMSContext context = Main.context;
            System.out.println("Je null");
            if (context!=null)
            {
                System.out.println("Nije null");
                JMSConsumer consumerTimer = context.createConsumer(Main.idTopic,"destinacija = 'timer'");
                JMSProducer producer = context.createProducer();
                JMSConsumer consumerPozadina = context.createConsumer(Main.idTopic,"destinacija = 'korisnikTimer'");
                Message message = consumerTimer.receive();

                String id = Main.requestStatePanel.getID();
                if (!id.equals(""))
                {
                    System.out.println("Nije prazan string");
                    ObjectMessage objMsg = context.createObjectMessage();
                    try {
                        objMsg.setObject(id);
                        objMsg.setStringProperty("operacija", "stanjeZahteva");
                        objMsg.setStringProperty("destinacija", "pozadina");
                        objMsg.setBooleanProperty("Timer", true);
                        producer.send(Main.idTopic,objMsg);

                        System.out.println("Cekam pozadinu Timer");
                        message = consumerPozadina.receive();
                        System.out.println("Pozadina odradila svoje");
                        DocumentResponse docRes = null;
                        if(message instanceof ObjectMessage)
                    {
                        //System.out.println("Primljena obj message");
                        ObjectMessage objReceivedMessage = (ObjectMessage)message;
                        docRes = objReceivedMessage.getBody(DocumentResponse.class);

                        if (objReceivedMessage.getStringProperty("uspeh").equals("d"))
                            {
                                Main.requestStatePanel.statusLabel.setText(docRes.getStatus());
                                if (docRes.getStatus().equals("cekaNaIzvrsenje"))
                                    Main.requestStatePanel.uruciButton.setEnabled(true);
                                Main.requestStatePanel.jmbgLabel.setText(docRes.getJMBGl());
                                Main.requestStatePanel.imeLabel.setText(docRes.getIme());
                                Main.requestStatePanel.prezimeLabel.setText(docRes.getPrezime());
                                Main.requestStatePanel.imeMajkeLabel.setText(docRes.getImeMajke());
                                Main.requestStatePanel.imeOcaLabel.setText(docRes.getImeOca());
                                Main.requestStatePanel.prezimeMajkeLabel.setText(docRes.getPrezimeMajke());
                                Main.requestStatePanel.prezimeOcaLabel.setText(docRes.getPrezimeOca());
                                Main.requestStatePanel.polLabel.setText(docRes.getPol());
                                DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");  
                                Main.requestStatePanel.datumLabel.setText(dateFormat.format(docRes.getDatumRodjenja()));
                                Main.requestStatePanel.nacionalnostLabel.setText(docRes.getNacionalnost());
                                Main.requestStatePanel.profesijaLabel.setText(docRes.getProfesija());
                                Main.requestStatePanel.bracnoLabel.setText(docRes.getBracnoStanje());
                                Main.requestStatePanel.opstinaLabel.setText(docRes.getOpstinaPrebivalista());
                                Main.requestStatePanel.ulicaLabel.setText(docRes.getUlicaPrebivalista());
                                Main.requestStatePanel.brojLabel.setText(docRes.getBrojPrebivalista());
                                Main.requestStatePanel.currid = id;
                                Main.rep();
                            }
                            else
                            {
                                JDialog neUspehDialog = new JDialog(Main.frame);
                                neUspehDialog.setAutoRequestFocus(true);
                                neUspehDialog.setTitle("Neuspeh");
                                neUspehDialog.setSize(200,60);
                                neUspehDialog.setResizable(false);
                                neUspehDialog.add(new JLabel("Dogodila se neka greska"));
                                neUspehDialog.setVisible(true);
                            }
            //            System.out.println(docRes);
                    }
                } catch (JMSException ex) {
                    Logger.getLogger(RequestStatePanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
            
        }
    }
}
