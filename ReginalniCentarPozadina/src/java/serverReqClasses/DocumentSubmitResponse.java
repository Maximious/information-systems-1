/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverReqClasses;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="DocumentSubmitResponse")
public class DocumentSubmitResponse implements Serializable
{
    //private static final long serialVersionUID = 3L;
     private String id;

    public DocumentSubmitResponse(String id) {
        this.id = id;
    }
    
    public DocumentSubmitResponse() {
    }

    public String getId() {
        return id;
    }

    @XmlElement(name="id")
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DocumentSubmitResponse{" + "id=" + id + '}';
    }
    
    
}
