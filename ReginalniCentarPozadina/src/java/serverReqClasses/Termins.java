package serverReqClasses;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Termins")
public class Termins implements Serializable
{
    //private static final long serialVersionUID = 6L;
    private List<Termin> termins = new ArrayList();

    public Termins() {
        
    }

    public Termins(List<Termin> termins) {
        this.termins = termins;
    }

    public Termins(String jsonArray)
    {
        ObjectMapper mapper = new ObjectMapper();
        Termin[] terms = null;
        try {
            terms = mapper.readValue(jsonArray, Termin[].class);
        } catch (IOException ex) {
            Logger.getLogger(Termins.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Termin term : terms) {
            termins.add(term);
        }
    }
    
    public List<Termin> getTermins() {
        return termins;
    }
    
    @XmlElement(name="termins")
    public void setTermins(List<Termin> termins) {
        this.termins = termins;
    }

    @Override
    public String toString() {
        StringBuilder sBuild = new StringBuilder();
        for (Termin t : termins)
        {
            sBuild.append(t.toString() + "\n");
        }
        return sBuild.toString();
    }
    
}