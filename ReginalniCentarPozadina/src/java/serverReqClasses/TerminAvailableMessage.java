/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverReqClasses;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="object")
public class TerminAvailableMessage implements Serializable
{   
   // private static final long serialVersionUID = 5L;
    private String poruka;
    //Dostupan termin

    public TerminAvailableMessage() {
    }

    public TerminAvailableMessage(String poruka) {
        this.poruka = poruka;
    }

    public String getPoruka() {
        return poruka;
    }

    @XmlElement(name="poruka")
    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }

    @Override
    public String toString() {
        return "TerminAvailableMessage{" + "poruka=" + poruka + '}';
    }
    
}
