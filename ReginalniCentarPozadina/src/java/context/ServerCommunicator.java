package context;

import com.fasterxml.jackson.databind.ObjectMapper;
import entities.Osoba;
import entities.Zahtevi;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import serverReqClasses.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response; 
import java.time.format.DateTimeFormatter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.client.Entity;


public class ServerCommunicator 
{
    
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("ReginalniCentarPozadinaPU");
    private static EntityManager em = emf.createEntityManager();

    public static void updateRequestState(String id) 
    {
        em.getTransaction().begin();
        Zahtevi postojiZahtev = em.find(Zahtevi.class,id);
        if (postojiZahtev != null)
            postojiZahtev.setStatus("urucen");
        em.persist(postojiZahtev);
        em.getTransaction().commit();
    }
    
    private ServerCommunicator() {}
    
    private static final String REG_ID = "17109";
    private static final String SERV_URI = "https://virtserver.swaggerhub.com/petar.noki0x60/ETFTask/1.0.0";
    private static final Client CLIENT = ClientBuilder.newClient();
    
    public static DocumentSubmitResponse submitRequest(DocumentRequest docRequest)
    {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
        LocalDateTime now = LocalDateTime.now();
        String datum = now.toString();
        datum = datum.replaceFirst("\\..*", "-07:00");
   
        int possible = checkTimeslotAvailability(datum);
        if (possible == 0)
        {
            System.out.println("Termin je trenutno zauzet!!!");
            return null;
        }
        
        WebTarget webTarget = CLIENT.target(SERV_URI);
        WebTarget employeeWebTarget = webTarget.path("/persoCentar/submit").queryParam("regionalniCentarId", REG_ID);
        Response response = employeeWebTarget.request().post(Entity.entity(docRequest,"application/xml"));
        //Bog samo zna zasto ovo vraca JSONA
        
        int status = response.getStatus();
        
        if (status == 400)
        {
            System.out.println("Nevalidan id dokumenta!");
            response.close();
            return null;
        }
        
        if (status == 201)
        {
            String txt = response.readEntity(String.class);

            ObjectMapper mapper = new ObjectMapper();
        
            DocumentSubmitResponse dsr=null;
            try {
                dsr = mapper.readValue(txt, DocumentSubmitResponse.class);
            } catch (IOException ex) {
                Logger.getLogger(Termins.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //System.out.println(dsr);
            
            //System.out.println(dsr.getId());
            response.close();
            
            docRequest.setId(dsr.getId());
            
            Osoba osb = new Osoba(docRequest);
            Zahtevi zah = new Zahtevi(dsr, osb);
            
            em.getTransaction().begin();
            Osoba postojiOsoba = em.find(Osoba.class, osb.getJmbg());
            if (postojiOsoba == null)
                em.persist(osb);
            Zahtevi postojiZahtev = em.find(Zahtevi.class,zah.getId());
            if (postojiZahtev == null)
                em.persist(zah);
            em.getTransaction().commit();
            return dsr;
        }

        System.out.println("Neocekivana greska");        
        response.close();
        return null;
    }
    
    public static DocumentResponse getReqStatus(String dokumentID)
    {
        WebTarget webTarget = CLIENT.target(SERV_URI);
        WebTarget employeeWebTarget = webTarget.path("/persoCentar/{dokumentId}").resolveTemplate("dokumentId", dokumentID).queryParam("regionalniCentarId", REG_ID);
        Invocation.Builder invocationBuilder = employeeWebTarget.request(MediaType.APPLICATION_XML);
        Response response = invocationBuilder.get();
        
        int status = response.getStatus();
        if (status == 400)
        {
            System.out.println("Nevalidan id dokumenta!");
            response.close();
            return null;
        }
        
        if (status == 404)
        {
            System.out.println("Dokument nije nadjen!");
            response.close();
            return null;
        }
         
        if (status == 200)
        {
            DocumentResponse txt = response.readEntity(DocumentResponse.class);
            if (txt.getStatus().equals("proizveden"))
            {
                txt.setStatus("cekaNaIzvrsenje");
                em.getTransaction().begin();
                Zahtevi postojiZahtev = em.find(Zahtevi.class,txt.getId());
                if (postojiZahtev != null)
                {
                    postojiZahtev.setStatus("cekaNaIzvrsenje");
                    em.persist(postojiZahtev);
                }
                em.getTransaction().commit();
            }
            System.out.println(txt);
            return txt;
        }
        else
        {
            System.out.println("Desila se nepredvidjena greska!");
        }
        
        response.close();
        return null;
    }
    
    public static Termins getAvailableTimeslots(String date)
    {
        //parametar dan
        WebTarget webTarget = CLIENT.target(SERV_URI);
        WebTarget employeeWebTarget = webTarget.path("/terminCentar/getAvailableTimeslots").queryParam("regionalniCentarId", REG_ID)
                    .queryParam("dan", date);
        Invocation.Builder invocationBuilder = employeeWebTarget.request();
        Response response = invocationBuilder.get();
        
        String txt = response.readEntity(String.class);
        
        Termins tms = new Termins(txt);
        System.out.println(tms);
        
        response.close();
        return tms;
    }
    
    public static int checkTimeslotAvailability(String date)
    { //YYYY:MM::DDThh:mm:ss.sTZD
        WebTarget webTarget = CLIENT.target(SERV_URI);
        WebTarget employeeWebTarget = webTarget.path("/terminCentar/checkTimeslotAvailability").queryParam("regionalniCentarId", REG_ID);
        Invocation.Builder invocationBuilder = employeeWebTarget.queryParam("termin",date/* "2020-01-20T13:50:00-07:00"*/).request(MediaType.APPLICATION_XML);
        Response response = invocationBuilder.get();
        
        int status = response.getStatus();
        
        if (status == 400)
        {
            System.out.println("Nevalidan parametar!");
            response.close();
            return -1;
        }
        
        if (status == 200)
        {
            TerminAvailableMessage txt = response.readEntity(TerminAvailableMessage.class);
            System.out.println(txt);
            response.close();
            if (txt.equals("Dostupan termin"))
                return 1;
            return 0;
        }
        
        System.out.println("Desila se nepredvidjena greska!");
        response.close();
        return -2;
        
    }
    
}
