/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import serverReqClasses.DocumentRequest;

/**
 *
 * @author Vol
 */
@Entity
@Table(name = "osoba")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Osoba.findAll", query = "SELECT o FROM Osoba o"),
    @NamedQuery(name = "Osoba.findByJmbg", query = "SELECT o FROM Osoba o WHERE o.jmbg = :jmbg"),
    @NamedQuery(name = "Osoba.findByIme", query = "SELECT o FROM Osoba o WHERE o.ime = :ime"),
    @NamedQuery(name = "Osoba.findByPrezime", query = "SELECT o FROM Osoba o WHERE o.prezime = :prezime"),
    @NamedQuery(name = "Osoba.findByDatumRodjenja", query = "SELECT o FROM Osoba o WHERE o.datumRodjenja = :datumRodjenja"),
    @NamedQuery(name = "Osoba.findByPol", query = "SELECT o FROM Osoba o WHERE o.pol = :pol"),
    @NamedQuery(name = "Osoba.findByBracnoStanje", query = "SELECT o FROM Osoba o WHERE o.bracnoStanje = :bracnoStanje"),
    @NamedQuery(name = "Osoba.findByImeMajke", query = "SELECT o FROM Osoba o WHERE o.imeMajke = :imeMajke"),
    @NamedQuery(name = "Osoba.findByPrezimeMajke", query = "SELECT o FROM Osoba o WHERE o.prezimeMajke = :prezimeMajke"),
    @NamedQuery(name = "Osoba.findByImeOca", query = "SELECT o FROM Osoba o WHERE o.imeOca = :imeOca"),
    @NamedQuery(name = "Osoba.findByPrezimeOca", query = "SELECT o FROM Osoba o WHERE o.prezimeOca = :prezimeOca"),
    @NamedQuery(name = "Osoba.findByNacionalnost", query = "SELECT o FROM Osoba o WHERE o.nacionalnost = :nacionalnost"),
    @NamedQuery(name = "Osoba.findByProfesija", query = "SELECT o FROM Osoba o WHERE o.profesija = :profesija"),
    @NamedQuery(name = "Osoba.findByOpstinaPrebivalista", query = "SELECT o FROM Osoba o WHERE o.opstinaPrebivalista = :opstinaPrebivalista"),
    @NamedQuery(name = "Osoba.findByUlicaPrebivalista", query = "SELECT o FROM Osoba o WHERE o.ulicaPrebivalista = :ulicaPrebivalista"),
    @NamedQuery(name = "Osoba.findByBrojPrebivalista", query = "SELECT o FROM Osoba o WHERE o.brojPrebivalista = :brojPrebivalista")})
public class Osoba implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ime")
    private String ime;
    @Basic(optional = false)
    @NotNull()
    @Size(min = 1, max = 50)
    @Column(name = "prezime")
    private String prezime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datumRodjenja")
    @Temporal(TemporalType.DATE)
    private Date datumRodjenja;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "pol")
    private String pol;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "bracnoStanje")
    private String bracnoStanje;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "imeMajke")
    private String imeMajke;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "prezimeMajke")
    private String prezimeMajke;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "imeOca")
    private String imeOca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "prezimeOca")
    private String prezimeOca;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nacionalnost")
    private String nacionalnost;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "profesija")
    private String profesija;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "opstinaPrebivalista")
    private String opstinaPrebivalista;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ulicaPrebivalista")
    private String ulicaPrebivalista;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "brojPrebivalista")
    private String brojPrebivalista;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "JMBG")
    private String jmbg;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "osobaID")
    private Collection<Zahtevi> zahteviCollection;

    public Osoba() {
    }

    public Osoba(String jmbg) {
        this.jmbg = jmbg;
    }
    
    public Osoba(DocumentRequest docReq)
    {
        this.jmbg = docReq.getJMBG();
        this.ime = docReq.getIme();
        this.prezime = docReq.getPrezime();
        this.datumRodjenja = docReq.getDatumRodjenja();
        this.pol = docReq.getPol();
        this.bracnoStanje = docReq.getBracnoStanje();
        this.imeMajke = docReq.getImeMajke();
        this.prezimeMajke = docReq.getPrezimeMajke();
        this.imeOca = docReq.getImeOca();
        this.prezimeOca = docReq.getPrezimeOca();
        this.nacionalnost = docReq.getNacionalnost();
        this.profesija = docReq.getProfesija();
        this.opstinaPrebivalista = docReq.getOpstinaPrebivalista();
        this.ulicaPrebivalista = docReq.getUlicaPrebivalista();
        this.brojPrebivalista = docReq.getBrojPrebivalista();
    }

    public Osoba(String jmbg, String ime, String prezime, Date datumRodjenja, String pol, String bracnoStanje, String imeMajke, String prezimeMajke, String imeOca, String prezimeOca, String nacionalnost, String profesija, String opstinaPrebivalista, String ulicaPrebivalista, String brojPrebivalista) {
        this.jmbg = jmbg;
        this.ime = ime;
        this.prezime = prezime;
        this.datumRodjenja = datumRodjenja;
        this.pol = pol;
        this.bracnoStanje = bracnoStanje;
        this.imeMajke = imeMajke;
        this.prezimeMajke = prezimeMajke;
        this.imeOca = imeOca;
        this.prezimeOca = prezimeOca;
        this.nacionalnost = nacionalnost;
        this.profesija = profesija;
        this.opstinaPrebivalista = opstinaPrebivalista;
        this.ulicaPrebivalista = ulicaPrebivalista;
        this.brojPrebivalista = brojPrebivalista;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }
    @XmlTransient
    public Collection<Zahtevi> getZahteviCollection() {
        return zahteviCollection;
    }
    public void setZahteviCollection(Collection<Zahtevi> zahteviCollection) {
        this.zahteviCollection = zahteviCollection;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jmbg != null ? jmbg.hashCode() : 0);
        return hash;
    }
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Osoba)) {
            return false;
        }
        Osoba other = (Osoba) object;
        if ((this.jmbg == null && other.jmbg != null) || (this.jmbg != null && !this.jmbg.equals(other.jmbg))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "entities.Osoba[ jmbg=" + jmbg + " ]";
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Date getDatumRodjenja() {
        return datumRodjenja;
    }

    public void setDatumRodjenja(Date datumRodjenja) {
        this.datumRodjenja = datumRodjenja;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getBracnoStanje() {
        return bracnoStanje;
    }

    public void setBracnoStanje(String bracnoStanje) {
        this.bracnoStanje = bracnoStanje;
    }

    public String getImeMajke() {
        return imeMajke;
    }

    public void setImeMajke(String imeMajke) {
        this.imeMajke = imeMajke;
    }

    public String getPrezimeMajke() {
        return prezimeMajke;
    }

    public void setPrezimeMajke(String prezimeMajke) {
        this.prezimeMajke = prezimeMajke;
    }

    public String getImeOca() {
        return imeOca;
    }

    public void setImeOca(String imeOca) {
        this.imeOca = imeOca;
    }

    public String getPrezimeOca() {
        return prezimeOca;
    }

    public void setPrezimeOca(String prezimeOca) {
        this.prezimeOca = prezimeOca;
    }

    public String getNacionalnost() {
        return nacionalnost;
    }

    public void setNacionalnost(String nacionalnost) {
        this.nacionalnost = nacionalnost;
    }

    public String getProfesija() {
        return profesija;
    }

    public void setProfesija(String profesija) {
        this.profesija = profesija;
    }

    public String getOpstinaPrebivalista() {
        return opstinaPrebivalista;
    }

    public void setOpstinaPrebivalista(String opstinaPrebivalista) {
        this.opstinaPrebivalista = opstinaPrebivalista;
    }

    public String getUlicaPrebivalista() {
        return ulicaPrebivalista;
    }

    public void setUlicaPrebivalista(String ulicaPrebivalista) {
        this.ulicaPrebivalista = ulicaPrebivalista;
    }

    public String getBrojPrebivalista() {
        return brojPrebivalista;
    }

    public void setBrojPrebivalista(String brojPrebivalista) {
        this.brojPrebivalista = brojPrebivalista;
    }
    
}
