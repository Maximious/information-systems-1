/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import serverReqClasses.DocumentSubmitResponse;

/**
 *
 * @author Vol
 */
@Entity
@Table(name = "zahtevi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Zahtevi.findAll", query = "SELECT z FROM Zahtevi z"),
    @NamedQuery(name = "Zahtevi.findById", query = "SELECT z FROM Zahtevi z WHERE z.id = :id"),
    @NamedQuery(name = "Zahtevi.findByStatus", query = "SELECT z FROM Zahtevi z WHERE z.status = :status")})
public class Zahtevi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "id")
    private String id;
    @Size(max = 15)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "osobaID", referencedColumnName = "JMBG")
    @ManyToOne(optional = false)
    private Osoba osobaID;

    public Zahtevi() {
    }

    public Zahtevi(String id) {
        this.id = id;
    }
    
    public Zahtevi(DocumentSubmitResponse dsr, Osoba osb)
    {
        id = dsr.getId();
        osobaID = osb;
        status = "uProdukciji";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Osoba getOsobaID() {
        return osobaID;
    }

    public void setOsobaID(Osoba osobaID) {
        this.osobaID = osobaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zahtevi)) {
            return false;
        }
        Zahtevi other = (Zahtevi) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Zahtevi[ id=" + id + " ]";
    }
    
}
