/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import context.ServerCommunicator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import serverReqClasses.DocumentRequest;
import serverReqClasses.DocumentResponse;
import serverReqClasses.DocumentSubmitResponse;
import serverReqClasses.Termins;





public class Main
{
    
    @Resource(lookup = "jms/__defaultConnectionFactory")
    public static ConnectionFactory connectionFactory;
    /*
    @Resource(lookup = "idQueueIn")
    public static Queue idQueueIn;
    
    @Resource(lookup = "idQueueOut2")
    public static Queue idQueueOut;
    */
    @Resource(lookup = "idTopic")
    public static Topic idTopic;
    
    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("ReginalniCentarPozadinaPU");
    public static EntityManager em = emf.createEntityManager();
    
    public static void main(String[] args) 
    {
        Destination destinationOut = idTopic;
        JMSContext context = connectionFactory.createContext();
        JMSConsumer consumer = context.createConsumer(idTopic, "destinacija = 'pozadina'");  
        JMSProducer producer = context.createProducer();
        ObjectMessage returnMsg = null;
        
        while (true)
        {
             try {
                System.out.println("Cekam poruku");
                Message message = consumer.receive();
                
                if (message instanceof ObjectMessage)
                {
                    ObjectMessage objMsg = (ObjectMessage)message;
                    String tipPoruke = objMsg.getStringProperty("operacija");
                    
                    switch(tipPoruke) 
                    { 
                        case "slanjeZahteva": 
                            System.out.println("slanjeZahteva"); 
                            DocumentRequest docReq = objMsg.getBody(DocumentRequest.class);
                            DocumentSubmitResponse docSubRes = ServerCommunicator.submitRequest(docReq);
                            System.out.println("Poslao: " + docReq);
                            System.out.println("Primio: " + docSubRes);
                            returnMsg = context.createObjectMessage();
                            returnMsg.setObject(docSubRes);
                            returnMsg.setStringProperty("destinacija", "korisnik");
                            if (docSubRes == null)
                                returnMsg.setStringProperty("uspeh", "=n");
                            else
                                returnMsg.setStringProperty("uspeh", "d"); //da, ne
                            producer.send(destinationOut, returnMsg);
                            break; 
                        case "stanjeZahteva": 
                            System.out.println("stanjeZahteva"); 
                            String idZahteva = objMsg.getBody(String.class);
                            DocumentResponse docRes = ServerCommunicator.getReqStatus(idZahteva);
                            System.out.println("Poslao: " + idZahteva);
                            System.out.println("Primio: " + docRes);
                            returnMsg = context.createObjectMessage();
                            returnMsg.setObject(docRes);
                            if (objMsg.getBooleanProperty("Timer"))
                                returnMsg.setStringProperty("destinacija", "korisnikTimer");
                            else
                                returnMsg.setStringProperty("destinacija", "korisnik");
                            if (docRes == null)
                                returnMsg.setStringProperty("uspeh", "n");
                            else
                                returnMsg.setStringProperty("uspeh", "d"); //da, ne
                            producer.send(destinationOut, returnMsg);
                            break; 
                        case "dostupniTermini": 
                            System.out.println("dostupniTermini"); 
                            String datum = objMsg.getBody(String.class);
                            Termins terms = ServerCommunicator.getAvailableTimeslots(datum);
                            System.out.println("Poslao: " + datum);
                            System.out.println("Primio: " + terms);
                            returnMsg = context.createObjectMessage();
                            returnMsg.setObject(terms);
                            returnMsg.setStringProperty("uspeh", "d"); //da, ne
                            returnMsg.setStringProperty("destinacija", "korisnik");
                            producer.send(destinationOut, returnMsg);
                            break;  
                        case "updateStanjeZahteva":
                            System.out.println("updateStanjeZahteva"); 
                            String id = objMsg.getBody(String.class);
                            ServerCommunicator.updateRequestState(id);
                            System.out.println("update status zahteva sa id: " + id);
                            break;  
                        default: 
                            System.out.println("Ne prepoznaje se tip poruke!"); 
                    } 
                    
                }

            } catch (JMSException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    
}
