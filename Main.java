/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 *
 * @author Vol
 */
public class Main {
    
    public static void main(String[] args)
    {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("https://virtserver.swaggerhub.com/petar.noki0x60/ETFTask/1.0.0");
        WebTarget employeeWebTarget = webTarget.path("/persoCentar/{dokumentId}").resolveTemplate("dokumentId", 2);
        Invocation.Builder invocationBuilder = employeeWebTarget.request(MediaType.APPLICATION_XML);
        Response response = invocationBuilder.get();
        
       // Entitet ent = response.readEntity(Entitet.class);
        String txt = response.readEntity(String.class);
        System.out.println(txt);
     
        response.close();
    }
    
}
