/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.timerapp;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.ObjectMessage;
import javax.jms.Topic;

/**
 *
 * @author tasha
 */
@Stateless
public class Timer {
    
    @Resource(lookup = "jms/__defaultConnectionFactory")
    public ConnectionFactory connectionFactory;
        
    @Resource(lookup = "idTopic")
    public Topic topic;
    
    public JMSContext context;
    public JMSProducer producer;
    
    public int vr=0;
    
    @Schedule(second="*/15", minute = "*",  hour = "*", persistent = false)
    public void test(){
        if(connectionFactory!=null && topic!=null){
            if(context==null){
                context=connectionFactory.createContext();
                producer=context.createProducer();
            }
            System.out.println("Cao ja sam tajmer");
            ObjectMessage objMsg = context.createObjectMessage();
            try {
                objMsg.setStringProperty("destinacija","timer");
            } catch (JMSException ex) {
                Logger.getLogger(Timer.class.getName()).log(Level.SEVERE, null, ex);
            }
            producer.send(topic,objMsg);
        }
    }
}

