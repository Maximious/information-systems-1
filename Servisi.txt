/persoCentar/submit:	

			
/persoCentar/{dokumentId}:			radi

public class Main {
    
    public static void main(String[] args)
    {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("https://virtserver.swaggerhub.com/petar.noki0x60/ETFTask/1.0.0");
        WebTarget employeeWebTarget = webTarget.path("/persoCentar/{dokumentId}").resolveTemplate("dokumentId", 2).queryParam("regionalniCentarId", "17109");
        Invocation.Builder invocationBuilder = employeeWebTarget.request();
        Response response = invocationBuilder.get();
        
       // Entitet ent = response.readEntity(Entitet.class);
        String txt = response.readEntity(String.class);
        System.out.println(txt);
     
        response.close();
    }
    
}

/terminCentar/getAvailableTimeslots		radi

public class Main {
    
    public static void main(String[] args)
    {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("https://virtserver.swaggerhub.com/petar.noki0x60/ETFTask/1.0.0");
        WebTarget employeeWebTarget = webTarget.path("/terminCentar/getAvailableTimeslots").queryParam("regionalniCentarId", "17109");
        Invocation.Builder invocationBuilder = employeeWebTarget.request();
        Response response = invocationBuilder.get();
        
       // Entitet ent = response.readEntity(Entitet.class);
        String txt = response.readEntity(String.class);
        System.out.println(txt);
     
        response.close();
    }
    
}

/terminCentar/checkTimeslotAvailability		radi	

public class Main {
    
    public static void main(String[] args)
    {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("https://virtserver.swaggerhub.com/petar.noki0x60/ETFTask/1.0.0");
        WebTarget employeeWebTarget = webTarget.path("/terminCentar/checkTimeslotAvailability").queryParam("regionalniCentarId", "17109");
        Invocation.Builder invocationBuilder = employeeWebTarget.queryParam("termin", "2020-01-20T13:50:00-07:00").request();
        Response response = invocationBuilder.get();
        
        //Entitet ent = response.readEntity(Entitet.class);
        String txt = response.readEntity(String.class);
        
        //System.out.println(ent);
        System.out.println(txt);
     
        response.close();
    }
    
}